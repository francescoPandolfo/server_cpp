//#include <iostream>

File::File(std::string codSt, const std::string nome, std::string dataCreazione, float frequenza, std::string inizioPeriodo, std::string finePeriodo){
    this->codStazione   = codSt;
    this->nome          = nome;
    this->dataCreazione = dataCreazione;
    this->frequenza     = frequenza;
    this->inizioPeriodo = inizioPeriodo;
    this->finePeriodo   = finePeriodo;
};


File::~File(){ std::cout << "Chiamato il distruttore di File" << std::endl; };

void File::storeDatabase(){
    DAOFactory_MySQL* factory = DAOFactory_MySQL::getInstance();
	factory->salvaMetadatiFile(this);

    MovimentiFile* mf = new MovimentiFile(GNSShare::getInstance()->utenteCorrente->username, "UPL",this->nome);
    factory->salvaMovimentiFile(mf);
};

void File::incrementaDownload(){
    DAOFactory_MySQL* factory = DAOFactory_MySQL::getInstance();
    factory->incrementaDownload(this);

    MovimentiFile* mf = new MovimentiFile(GNSShare::getInstance()->utenteCorrente->username, "DWL",this->nome);
    factory->salvaMovimentiFile(mf);
}

void File::stampa(){
    std::cout << this->to_string() << std::endl;
}

std::string File::to_string() const{
    return 
"   Nome: " + this->nome + "\n" + 
"   data creazione: " + this->dataCreazione + "\n" + 
"   frequenza: " + std::to_string(this->frequenza) + "\n" + 
"   dal " + this->inizioPeriodo + " al " + this->finePeriodo;
};

std::string* File::to_JSON(bool kv = false) const{
	return new std::string("{nome:\""+nome+"\",dataCreazione:\""+dataCreazione+"\",inizioPeriodo:\""+inizioPeriodo+"\",finePeriodo:\""+finePeriodo+"\",frequenza:"+std::to_string(frequenza)+"}");
};
