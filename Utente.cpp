Utente::Utente(const std::string& username, std::string& password, std::string& nome, std::string& cognome, std::string& ente, role ruolo){
    //std::cout << "Chiamato il costruttore della classe Utente" << std::endl;
    this->username  = username;
    this->password  = password;
    this->nome      = nome;
    this->cognome   = cognome;
    this->ente      = ente;
    this->ruolo     = ruolo;
}

Utente::~Utente(){
    std::cout << "Chiamato il distruttore della classe Utente" << std::endl;
}

void Utente::storeDatabase(){
    DAOFactory_MySQL* factory = DAOFactory_MySQL::getInstance();
	factory->salvaUtente(this);
};
 
std::string Utente::to_string() const{
    return utility::textColor_green(this->username) + " (" + this->nome + " " + this->cognome + ")\nEnte di appartenenza: " + this->ente + "\t\tRuolo: " + Utente::role_names.at(this->ruolo);
};

void Utente::stampa(){
	std::cout << "[Utente] " << this->to_string() << std::endl;
};