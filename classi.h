#include <map>
#include <list>
#include <iostream>
#include <bits/stdc++.h>	//per creare i vector
#include "utility.cpp"
#include "persistence/persistence.include"
#include "include/HTTPRequest.hpp"

class Utente{
public:
	enum role { ADMIN, EDITOR, VIEWER, NOT_LOGGED }; //assicurarsi che NOT_LOGGET stia sempre alla fine
	static std::vector<std::string> role_names; //utilizzato solo per la stampa a video (nuovoUtente)
	
//attributi
	std::string username, password, nome, cognome, ente;
	role ruolo = role::NOT_LOGGED; //valore di default
//costruttori
	Utente(){};
	Utente(const std::string&, std::string&, std::string&, std::string&, std::string&, role);
	~Utente();
//metodi
	void storeDatabase();
	std::string to_string() const;
	void stampa();
};

std::vector<std::string> Utente::role_names = { "ADMIN", "EDITOR", "VIEWER" };

class File{
public:
	std::string codStazione, nome, dataCreazione, inizioPeriodo, finePeriodo;
	float frequenza;
//costruttori
	File(){};
	File(std::string, const std::string, std::string, float, std::string, std::string);
	~File();
//metodi
	void storeDatabase();
	void incrementaDownload();
	void stampa();
	std::string to_string() const;
	virtual std::string* to_JSON(bool) const;
};

class MovimentiFile{
public:
	std::string username, tipoMovimento, nomeFile;
//costruttori
	MovimentiFile();
	MovimentiFile(std::string usr, std::string tipo, std::string nf){this->username = usr;this->tipoMovimento = tipo;this->nomeFile = nf;};
	~MovimentiFile();
};


class Stazione{

protected:
	virtual void storeDatabase(){};

public:
	float latitudine, longitudine, altitudine;
	std::string codice, nome;
	std::map<std::string, File*>* elencoFile;

//costruttori
	Stazione();
	Stazione(const std::string codice, float lat, float lng, float alt);
	Stazione(std::string codice, std::string nome, float lat, float lng, float alt);
	virtual ~Stazione();
//metodi
	virtual void stampa();
	virtual std::string to_string() const;
	std::string* uploadFile(std::string&, float&, std::string&, std::string&, std::string&);
	void downloadFile(std::string);
};


class StazioneGNSS : public Stazione{
public:
	std::string rete, tipo;
	bool gps, galileo, glonass;

//costruttori
	StazioneGNSS();
	StazioneGNSS(const std::string, std::string, float, float, float, std::string, std::string, bool, bool, bool);
	~StazioneGNSS();
//metodi
	void update(std::string, std::string, float, float, float, std::string, std::string, bool, bool, bool);
	void stampa();
	std::string to_string() const override;
	virtual std::string* to_JSON(bool) const;
	void storeDatabase();
};

class GNSShare{

	static GNSShare* instance;
	std::map<std::string, StazioneGNSS*>* elencoStazioni;
	

protected:
	GNSShare();
	void stampaHeader(std::string);
	void stampaUtenti();
	void stampaStazioniGNSS();

//metodi protetti, sottoposti ad Autenticazione
//così che se la classe derivata non li sovrascrive vengono eseguiti dalla factory

	//utilizzati se chiamata l'interazione da console
	virtual void _login();
	virtual void _logout();
	virtual void _nuovoUtente();
	virtual void _nuovaStazioneGNSS();
	virtual void _uploadFile();

	//chiamati dall'endpoint: solo gli autorizzati possono eseguirli
	virtual void _login(std::string&, std::string&);
	virtual void _nuovoUtente(const std::string&, std::string&, std::string&, std::string&, std::string&, int&);
	virtual void _nuovaStazioneGNSS(const std::string&, std::string&, float&, float&, float&, std::string&, std::string&, bool&, bool&, bool&);
	virtual std::string* _uploadFile(std::string&, std::string&, float&, std::string&, std::string&, std::string&);
	virtual void _downloadFile(std::string&, std::string&);
	virtual std::string* _getStazioniGNSS(bool&);
	virtual void _stampaUtenti();

public:
	Utente* utenteCorrente;			//utilizzato solo se si utilizza l'interfaccia da console
	enum menu { AGGIUNGI_UTENTE, AGGIUNGI_STAZIONE, CARICA_FILE, ELENCO_STAZIONI, ELENCO_UTENTI, LOGIN, LOGOUT, ESCI, GET_STAZIONI, GET_MAPPA_STAZIONI, REFRESH_MAPPA_STAZIONI, DOWNLOAD_FILE, STATISTICHE};
	static std::vector<std::string> menu_names; //utilizzato solo per la stampa a video (nuovoUtente)
	std::map<menu, std::list<Utente::role> > permessi_menu;
	std::string* getEndpointsByRole(Utente::role);

	~GNSShare();

	static GNSShare* getInstance();
	void _avviamento(); //carica i dati dal database
	void menu();

//utilizzati se chiamata l'interazione da console
	void login();
	void nuovoUtente();
	void nuovaStazioneGNSS();
	void uploadFile();

//endpoints: fornire USERNAME e PASSWORD come primi due parametri, necessari per l'autenticazione
	void login(std::string, std::string, std::map<std::string, std::string>*);
	void logout(std::string, std::string, std::map<std::string, std::string>*); 
	void nuovoUtente(std::string, std::string, const std::string, std::string, std::string, std::string, std::string, int);
	void nuovaStazioneGNSS(std::string, std::string, const std::string, std::string, float, float, float, std::string, std::string, bool, bool, bool);	
	std::string* getStazioniGNSS(std::string, std::string, bool);
	std::string* uploadFile(std::string, std::string, std::string, std::string, float, std::string, std::string, std::string);
	void downloadFile(std::string, std::string, std::string, std::string);
	void stampaUtenti(std::string, std::string);
	std::vector<std::uint8_t>* sendRESTtoPython(std::string, std::string);
};

std::vector<std::string> GNSShare::menu_names = { "AGGIUNGI_UTENTE", "AGGIUNGI_STAZIONE", "CARICA_FILE", "ELENCO_STAZIONI", "ELENCO_UTENTI", "LOGIN", "LOGOUT", "ESCI", "GET_STAZIONI", "GET_MAPPA_STAZIONI", "REFRESH_MAPPA_STAZIONI", "DOWNLOAD_FILE", "STATISTICHE" };


class DAOFactory_MySQL final : public persistence::DAOFactory{
	
	static DAOFactory_MySQL* instance;
	DAOFactory_MySQL(); //costruttore privato per implementare il singleton

	Utente* utente; //saranno i metodi ad accedervi (salvaUtente)
	StazioneGNSS* stazioneGNSS;
	File* file;
	MovimentiFile* movimentiFile;

public:
	~DAOFactory_MySQL();

	static DAOFactory_MySQL* getInstance();
	void salvaUtente(Utente*);
	persistence::DAO::Utente* factoryUtente() override;
	void salvaStazioneGNSS(StazioneGNSS*);
	persistence::DAO::StazioneGNSS* factoryStazioneGNSS() override;
	void salvaMetadatiFile(File*);
	void incrementaDownload(File*);
	persistence::DAO::MetadatiFile* factoryMetadatiFile() override;
	persistence::DAO::MovimentiFile* factoryMovimentiFile() override;
	void salvaMovimentiFile(MovimentiFile*);
	
	std::map<std::string, Utente*>* caricaListaUtenti();
	std::map<std::string, StazioneGNSS*>* caricaListaStazioniGNSS();
	std::map<std::string, File*>* caricaListaFilePerStazione(std::string codiceStazione);
};

GNSShare* GNSShare::instance = nullptr;
DAOFactory_MySQL* DAOFactory_MySQL::instance = nullptr;