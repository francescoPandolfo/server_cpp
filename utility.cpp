namespace utility{
    #define ACCESSO_NEGATO "l'utente non ha i permessi per accedere a questa funzione!"

    static bool consoleMode = false;

    template<typename T>
    void stampaMappaDiOggetti(T &mappa){
        for (typename T::iterator it = mappa.begin(); it != mappa.end(); ++it){
            it->second->stampa(); //implementare il metodo stampa() per le classi che devono utilizzare questo template
            std::cout <<std::endl;
        }
    };

    template<typename T>
    void stampa_PuntatoreMappaDiOggetti(T* mappa){
        if (mappa == nullptr) return;
        for (typename T::iterator it = mappa->begin(); it != mappa->end(); ++it){
            //std::cout << "-> "<<  (*it).second << std::endl; //utile per esaminare la locazione di memoria
            (*it).second->stampa(); //implementare il metodo stampa() per le classi che devono utilizzare questo template
            std::cout <<std::endl;
        }
    };

    template<typename T>
    void stampaChiaviMappa(T &mappa){
        for (typename T::iterator it = mappa.begin(); it != mappa.end(); ++it)
            std::cout << "\t" << it->first << "\n";
    };

    template<typename T>
    void stampaChiavi_PuntatoreMappa(T* mappa){
        if (mappa == nullptr) return;
        for (typename T::iterator it = mappa->begin(); it != mappa->end(); ++it)
            std::cout << "\t" << (*it).first << "\n";
    };

    template<typename T>
    std::string* PuntatoreMappaDiOggetti_toJSON(T* mappa, bool kv = false){
        if (mappa == nullptr) return new std::string("[]");
        else{
            std::string* toReturn = new std::string("[");
            int count = 0;
            for (typename T::iterator it = mappa->begin(); it != mappa->end(); ++it){
                count++;
                *toReturn += (*(*it).second->to_JSON(kv)) + ","; //implementare il metodo to_JSON() per le classi che devono utilizzare questo template
            }
            if(count >0) *toReturn = toReturn->substr(0, toReturn->length() -1);
            *toReturn += "]";
            return toReturn;
        }
    };

    void eliminaCaratteriSpeciali (std::string* str, std::list<char> car){ //che fanno andare in arrore la risposta da mandare al client
	bool getOccurrence = false;
	//std::list<char> car = {':'};
	for (auto const& i : car){
		do{
			getOccurrence = false;
			//std::cout << *str << std::endl;
			size_t pos_elem = 0;
			pos_elem = str->find(i);
			if(pos_elem != std::string::npos){
				*str = str->substr(0,pos_elem) + str->substr(pos_elem + 1, str->length() - pos_elem );
				getOccurrence = true;
			}
		}
		while(getOccurrence);
	}
}

    std::string textColor_red(std::string Texting ){ return "\x1B[31m" + Texting + "\033[0m"; };
    std::string textColor_green(std::string Texting ){ return "\x1B[32m" + Texting + "\033[0m"; };
    std::string textColor_yellow(std::string Texting ){ return "\x1B[33m" + Texting + "\033[0m"; };

    void accessoNegato(){ 
        std::cout << utility::textColor_red(ACCESSO_NEGATO) << std::endl; 
        if (consoleMode){
            std::cout << "\n\nPremi un tasto per continuare... ";
            std::cin.ignore(); 
        }
    };

}