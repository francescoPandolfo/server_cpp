#include <exception>

#include "AuthService.cpp"
#include "Utente.cpp"
#include "DAOFactory_MySQL.cpp"
#include "Stazione.cpp"
#include "StazioneGNSS.cpp"
#include "File.cpp"
#include <bits/stdc++.h>


#include <list>
    
//metodi
/*Classe singleton: 
restituisce una nuova istanza se non presente oppure l'istanza già creata */

std::string STAZIONE_GIA_PRESENTE_A_SISTEMA = "Il codice stazione scelto e' gia' presente a sistema.";
std::string UTENTE_GIA_PRESENTE_A_SISTEMA = "L'username scelto e' gia' presente a sistema.";

GNSShare* GNSShare::getInstance(){
    if ( instance == nullptr ){
        instance = new GNSShare(); //creato dinamicamente
    }
    return instance;
};

GNSShare::GNSShare(){
    //std::cout << "Chiamato il costruttore di GNSShare" << std::endl;
    this->permessi_menu[menu::AGGIUNGI_UTENTE]      = { Utente::role::ADMIN };
    this->permessi_menu[menu::AGGIUNGI_STAZIONE]    = { Utente::role::ADMIN };
    this->permessi_menu[menu::CARICA_FILE]          = { Utente::role::ADMIN , Utente::role::EDITOR };
    this->permessi_menu[menu::ELENCO_STAZIONI]      = { Utente::role::ADMIN , Utente::role::EDITOR , Utente::role::VIEWER, Utente::role::NOT_LOGGED };
    this->permessi_menu[menu::ELENCO_UTENTI]        = { Utente::role::ADMIN };
    this->permessi_menu[menu::LOGIN]                = { Utente::role::NOT_LOGGED };
    this->permessi_menu[menu::LOGOUT]               = { Utente::role::ADMIN , Utente::role::EDITOR , Utente::role::VIEWER };
    this->permessi_menu[menu::ESCI]                 = { Utente::role::ADMIN , Utente::role::EDITOR , Utente::role::VIEWER, Utente::role::NOT_LOGGED };
    this->permessi_menu[menu::GET_STAZIONI]         = { Utente::role::ADMIN , Utente::role::EDITOR , Utente::role::VIEWER, Utente::role::NOT_LOGGED };
    this->permessi_menu[menu::GET_MAPPA_STAZIONI]   = { Utente::role::ADMIN , Utente::role::EDITOR , Utente::role::VIEWER, Utente::role::NOT_LOGGED };
    this->permessi_menu[menu::DOWNLOAD_FILE]        = { Utente::role::ADMIN , Utente::role::EDITOR , Utente::role::VIEWER };
    this->permessi_menu[menu::STATISTICHE]    = { Utente::role::ADMIN };
    
}
GNSShare::~GNSShare(){
    //std::cout << "Chiamato il distruttore di GNSShare" << std::endl;
};

void GNSShare::_avviamento(){
    
    std::map<std::string, Utente*>* momUt = DAOFactory_MySQL::getInstance()->caricaListaUtenti();
    AuthService::elencoUtenti = momUt; 
    momUt = nullptr; //al termine di _avviamento verrà cancellata comunque

    std::map<std::string, StazioneGNSS*>* momSt = DAOFactory_MySQL::getInstance()->caricaListaStazioniGNSS();
    getInstance()->elencoStazioni = momSt;    
};

void GNSShare::_login(){
    std::string username, password;
    stampaHeader("Login utente");
    std::cout << "\nInserisci l'username: ";
    getline(std::cin, username);
    std::cout << "\nInserisci la password: ";
    getline(std::cin, password);
    bool error = false;
    
    std::map<std::string, std::string>* reply = new std::map<std::string, std::string>();

    try{
        login(username, password, reply);
    }
    catch(char const* str){
        error = true;
    }
    catch(const std::exception &e){
        error = true;
    }
    delete reply; reply = nullptr;
    std::cout << "\n\nPremi un tasto per continuare... ";
    std::cin.ignore();
};

void GNSShare::_login(std::string& username, std::string& password){
    AuthService::login(username, password, getInstance());
};

std::string* GNSShare::getEndpointsByRole(Utente::role r){
    std::string* toReturn = new std::string("[");
    
    for (auto& it : permessi_menu){
        for (const auto& rr : it.second){
            if (rr == r){
                *toReturn += std::to_string(it.first) + ",";
                break;
            }
        }
    }
    toReturn->pop_back(); //rimuove l'ultimo carattere (la virgola)
    *toReturn += "]";
    return toReturn;
};


void GNSShare::_logout(){
    std::map<std::string, std::string>* reply = new std::map<std::string, std::string>();
    logout(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password, reply);
    delete reply; reply = nullptr;
};

void GNSShare::menu(){
    
    while(true){
        std::string str_menu = "";
        Utente::role ruolo_utente_corrente;

        if (getInstance()->utenteCorrente == nullptr) {
            getInstance()->utenteCorrente = new Utente();
            //ruolo_utente_corrente = Utente::role::NOT_LOGGED;
        }
        ruolo_utente_corrente = getInstance()->utenteCorrente->ruolo;
        
        for (auto& it : permessi_menu){

            for (const auto& rr : it.second){
                if (rr == ruolo_utente_corrente){
                    std::string descrMenu;
                    switch(it.first){
                        case menu::AGGIUNGI_UTENTE      : descrMenu = " - Aggiungi nuovo utente\n"; break;
                        case menu::AGGIUNGI_STAZIONE    : descrMenu = " - Aggiungi nuova stazione\n"; break;
                        case menu::CARICA_FILE          : descrMenu = " - Carica un nuovo file\n"; break;
                        case menu::ELENCO_STAZIONI      : descrMenu = " - Elenco stazioni\n"; break;
                        case menu::ELENCO_UTENTI        : descrMenu = " - Elenco utenti\n"; break;
                        case menu::LOGIN                : descrMenu = " - Login\n"; break;
                        case menu::LOGOUT               : descrMenu = " - Logout\n"; break;
                        case menu::ESCI                 : descrMenu = " - Esci\n"; break;
                    }
                    if (descrMenu != "") str_menu += std::to_string(it.first) += descrMenu;
                    break;
                }
            }
        }
        
        str_menu += "\nCosa segli? ";
        int scelta;
        try{
            stampaHeader("Menu principale");
            std::cout << str_menu;
            std::cin >> scelta;
            std::cin.ignore();
            switch(scelta){
                case menu::AGGIUNGI_UTENTE:     nuovoUtente(); break;
                case menu::AGGIUNGI_STAZIONE:   nuovaStazioneGNSS(); break;
                case menu::CARICA_FILE:         uploadFile(); break;
                case menu::ELENCO_STAZIONI:     stampaStazioniGNSS(); break;
                case menu::ELENCO_UTENTI:       stampaUtenti(); break;
                case menu::LOGIN:               login(); break;
                case menu::LOGOUT:              _logout(); break;
                case menu::ESCI:                return;
            }
        }
        catch(char* e){
            std::cout << utility::textColor_red("Si è verificato un errore: ") << std::endl;
        }
    }
    
};

void GNSShare::stampaHeader(std::string pagina){
    std::string loggedUser = "";
    if( getInstance()->utenteCorrente->ruolo != Utente::role::NOT_LOGGED ) loggedUser = getInstance()->utenteCorrente->to_string();
    system("clear");
    std::cout << std::string("\n***************************     GNSShare    ********************************\n") +
        "\t\t" + pagina + "\t\t" + loggedUser + "\n" +
        "****************************************************************************\n\n";
};

void GNSShare::_nuovoUtente(){
    std::string username, password, nome, cognome, ente;
    int ruolo;
    stampaHeader("Inserimento nuovo Utente");
    std::cout << "\nInserisci l'username: ";
    getline(std::cin, username);
    std::cout << "\nInserisci la password: ";
    getline(std::cin, password);
    std::cout << "\nInserisci il nome: ";
    getline(std::cin, nome);
    std::cout << "\nInserisci il cognome: ";
    getline(std::cin, cognome);
    std::cout << "\nInserisci l'ente di appartenenza: ";
    getline(std::cin, ente);
    std::cout << "\nInserisci il ruolo: " <<std::endl;
    
    for(Utente::role r = Utente::role::ADMIN; r != Utente::role::NOT_LOGGED; r=(Utente::role)(r+1))
            std::cout << r << " -> " << Utente::role_names.at(r) << std::endl;    
    
    std::cin >> ruolo;
    std::cin.ignore();

    Utente utenteCorr(username, password, nome, cognome, ente, Utente::role(ruolo)); //creato oggetto staticamente per richiamare metodo stampa: consuma memoria ma è più pratico
    
    std::cout << "\nConfermi l'inserimento del nuovo utente?\n\n";
    utenteCorr.stampa();
    std::cout << utility::textColor_yellow("\n\ns = conferma, n = non conferma, a = annulla\n>>");
    char risp = 'a';
    std::cin >> risp;
    std::cin.ignore();

    switch(risp){
        case 's': 
            try{
                nuovoUtente(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password,
                                utenteCorr.username, utenteCorr.password, utenteCorr.nome, utenteCorr.cognome, utenteCorr.ente,
                                int(utenteCorr.ruolo) );
            }
            catch(...){
                //std::exception_ptr p = std::current_exception();
                std::cout << "Errore in fase di salvataggio... utente non inserito!" << std::endl;
            }
            std::cout << "\n\nPremi un tasto per continuare... ";
            std::cin.ignore(); 
            break;
        case 'n': nuovoUtente(); break;
        case 'a':
        default: break;
    }
};

void GNSShare::_nuovoUtente(const std::string& username, std::string& password, std::string& nome, std::string& cognome, std::string& ente, int& ruolo){    
    Utente* nuovoUt = new Utente(username, password, nome, cognome, ente, Utente::role(ruolo));

    //salvo il nuovo utente nel database
    nuovoUt->storeDatabase();
    std::cout << utility::textColor_green("** Utente \"" + nuovoUt->username + "\" salvato nel database!") << std::endl;

    AuthService::elencoUtenti->insert( { username, nuovoUt });
    std::cout << utility::textColor_green("** Utente \"" + nuovoUt->username + "\" aggiunto a sistema!") << std::endl;
};

void GNSShare::_nuovaStazioneGNSS(){
    std::string codice, nome, rete, tipo;
    float latitudine, longitudine, altitudine;
    bool gps, glo, gal;

    stampaHeader("Inserimento nuova stazione");
    std::cout << "\nInserisci il codice: ";
    getline(std::cin, codice);
    
    std::cout << "\nInserisci il nome: ";
    getline(std::cin, nome);
    std::cout << "\nInserisci la latitudine: ";
    std::cin >> latitudine;
    std::cout << "\nInserisci la longitudine: ";
    std::cin >> longitudine;
    std::cout << "\nInserisci l'altitudine': ";
    std::cin >> altitudine;
    std::cin.ignore(); //lo inserisco perchè salta la lettura successiva
    std::cout << "\nInserisci la rete di appartenenza: ";
    getline(std::cin, rete);
    std::cout << "\nInserisci il tipo: ";
    getline(std::cin, tipo);
    std::cout << "\nRiceve GPS? (1/0) ";
    std::cin >> gps;
    std::cout << "\nRiceve GLONASS? (1/0) ";
    std::cin >> glo;
    std::cout << "\nRiceve GALILEO? (1/0) ";
    std::cin >> gal;

    StazioneGNSS stazioneCorrente(codice, nome, latitudine, longitudine, altitudine, rete, tipo, gps, glo, gal);

    std::cout << "\nConfermi l'inserimento della nuova stazione?\n\n";
    stazioneCorrente.stampa();
    std::cout << utility::textColor_yellow("\n\ns = conferma, n = non conferma, a = annulla\n>>");
    char risp = 'a';
    std::cin >> risp;
    std::cin.ignore();

    
        switch(risp){
            case 's': 
                try{
                    nuovaStazioneGNSS(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password,
                                stazioneCorrente.codice, stazioneCorrente.nome, stazioneCorrente.latitudine, stazioneCorrente.longitudine, stazioneCorrente.altitudine,
                                stazioneCorrente.rete, stazioneCorrente.tipo,
                                stazioneCorrente.gps, stazioneCorrente.galileo, stazioneCorrente.glonass);
                }
                catch(...){
                    //std::exception_ptr p = std::current_exception();
                    std::cout << "Errore in fase di salvataggio... stazione non inserita!" << std::endl;
                }
                std::cout << "\n\nPremi un tasto per continuare... ";
                std::cin.ignore();
                break;
            case 'n': nuovaStazioneGNSS(); break;
            case 'a':
            default: break;
        }
};

void GNSShare::_nuovaStazioneGNSS(const std::string& codice, std::string& nome, float& lat, float& lng, float& alt, 
        std::string& rete, std::string& tipo, bool& gps, bool& gal, bool& glo){
    
    StazioneGNSS *nuovaStazione = new StazioneGNSS(codice, nome, lat, lng, alt, rete, tipo, gps, gal, glo);

    //salvo la nuova stazione nel database
    nuovaStazione->storeDatabase();
    std::cout << utility::textColor_green("** Stazione GNSS \"" + nuovaStazione->codice + "\" salvata nel database!") << std::endl;

    /*la nuova stazione non deve essere aggiunta all'attuale istanza derivata (this->elencoStazioni) perchè sarebbe visibile solo all'utente corrente;
    aggiungendola all'elenco delle stazioni della classe base sarà visibile in tempo reale a tutti gli altri utenti connessi, dato che la classe base è condivisa (singleton)*/
    //(*(getInstance())->elencoStazioni)[codice] = nuovaStazione;
    (getInstance())->elencoStazioni->insert( { codice, nuovaStazione});
    std::cout << utility::textColor_green("** Stazione GNSS \"" + nuovaStazione->codice + "\" aggiunta a sistema!") << std::endl;
};

void GNSShare::_uploadFile(){
    std::string codiceStazione, dataOra, inizioPeriodo, finePeriodo;
    float frequenza;

    stampaHeader("Upload nuovo file");
    std::cout << "Elenco stazioni a sistema:\n\n";

    utility::stampaChiavi_PuntatoreMappa( getInstance()->elencoStazioni );
    
    bool esci = false;
    while(!esci){
        std::cout << "\n\nInserire il codice della stazione di cui si vuole caricare un nuovo file oppure digitare \"a\" per annullare l'operazione:\n";
        std::cout << utility::textColor_yellow(">>");
        getline(std::cin, codiceStazione);
        if (codiceStazione == "a") { esci = true; break; }

        if ( getInstance()->elencoStazioni->find(codiceStazione) == getInstance()->elencoStazioni->end() ){ 
            std::cout << "\nCodice stazione non esistente!";
        }
        else{
            std::cout << std::endl;
            getInstance()->elencoStazioni->at(codiceStazione)->stampa(); //richiamo il metodo stampa della stazione scelta
            
            std::cout << "\nInserire la data e ora in formato YYYY-MM-DD HH:mm:ss : ";
            getline(std::cin, dataOra);
            std::cout << "\nInserire la frequenza della serie temporale (es. 1 = 1Hz): ";
            std::cin >> frequenza;
            std::cin.ignore(); //lo inserisco perchè salta la lettura successiva
            std::cout << "\nInserire l'inizio del periodo in formato YYYY-MM-DD HH:mm:ss : ";
            getline(std::cin, inizioPeriodo);
            std::cout << "\nInserire la fine del periodo in formato YYYY-MM-DD HH:mm:ss : ";
            getline(std::cin, finePeriodo);

            std::cout << "\n\nConfermi l'inserimento del nuovo file?\n";
            std::cout << utility::textColor_yellow("s = conferma, n = non conferma, a = annulla\n>>");
            char risp = 'a';
            std::cin >> risp;
            std::cin.ignore();
            switch(risp){
                case 's': 
                    uploadFile(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password, codiceStazione, dataOra, frequenza, inizioPeriodo, finePeriodo, "");
                    std::cout << "\n\nPremi un tasto per continuare... ";
                    std::cin.ignore();
                    esci = true;
                    break;
                case 'n': break;
                case 'a': esci = true; break;
                default: break;
            };
        }
    }
    
};

std::string* GNSShare::_uploadFile(std::string& codiceStazione, std::string& dataOra, float& frequenza, std::string& inizioPeriodo, std::string& finePeriodo, std::string& estensione){
    if(!getInstance()->elencoStazioni->count(codiceStazione)) throw "Il codice stazione indicato non e' presente a sistema";
    StazioneGNSS* st = getInstance()->elencoStazioni->at(codiceStazione); //puntiamo alla stazione desiderata
    return st->uploadFile(dataOra, frequenza, inizioPeriodo, finePeriodo, estensione);
};


void GNSShare::_downloadFile(std::string& codiceStazione, std::string& nome){
    if(!getInstance()->elencoStazioni->count(codiceStazione)) throw "Il codice stazione indicato non e' presente a sistema";
    StazioneGNSS* st = getInstance()->elencoStazioni->at(codiceStazione); //puntiamo alla stazione desiderata
    st->downloadFile(nome);
};

void GNSShare::stampaUtenti(){
    stampaUtenti(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password);
};

void GNSShare::_stampaUtenti(){
    stampaHeader("Elenco Utenti");
    utility::stampa_PuntatoreMappaDiOggetti( AuthService::elencoUtenti );
    std::cout << "\n\nPremi un tasto per continuare... ";
    std::cin.ignore();
};

void GNSShare::stampaStazioniGNSS(){
    stampaHeader("Elenco stazioni GNSS");
    utility::stampa_PuntatoreMappaDiOggetti( getInstance()->elencoStazioni );
    std::cout << "\n\nPremi un tasto per continuare... ";
    std::cin.ignore();
};

std::string* GNSShare::_getStazioniGNSS(bool& kv){
    return utility::PuntatoreMappaDiOggetti_toJSON( getInstance()->elencoStazioni, kv );
};

/*************** CON AUTENTICAZIONE ********************/

void GNSShare::login(){
    GNSShare* factory = AuthService::factoryGNSShare(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password);
    factory->_login();
    delete factory;
};

void GNSShare::nuovoUtente(){
    GNSShare* factory = AuthService::factoryGNSShare(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password);
    factory->_nuovoUtente();
    delete factory;
    factory = nullptr;
};

void GNSShare::nuovaStazioneGNSS(){
    GNSShare* factory = AuthService::factoryGNSShare(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password);
    factory->_nuovaStazioneGNSS();
    delete factory;
};

void GNSShare::uploadFile(){
    GNSShare* factory = AuthService::factoryGNSShare(getInstance()->utenteCorrente->username, getInstance()->utenteCorrente->password);
    factory->_uploadFile();
    delete factory;
};

//ENDPOINTS

void GNSShare::login(std::string username, std::string password, std::map<std::string, std::string>* reply){
    GNSShare* factory = AuthService::factoryGNSShare();
    try{
        factory->_login(username, password);
    }
    catch(char const* str){
        delete factory; factory = nullptr;
        throw str;
    }
    catch(const std::exception &e){
        delete factory; factory = nullptr;
        throw e.what();
    }    
    delete factory; factory = nullptr;

    (*reply)["operationCompleted"] = "1";
    (*reply)["isAuthenticated"] = "1";
    (*reply)["role"] = std::to_string( getInstance()->utenteCorrente == nullptr ? Utente::role::NOT_LOGGED : (getInstance()->utenteCorrente)->ruolo );
    (*reply)["endpoints"] = *(getInstance()->getEndpointsByRole( getInstance()->utenteCorrente == nullptr ? Utente::role::NOT_LOGGED : (getInstance()->utenteCorrente)->ruolo ));
    //se l'utente è stato autenticato restituisco al client i dati sull'utente
    
    std::string userInfo = "{nome:\"" + getInstance()->utenteCorrente->nome + "\",cognome:\"" + getInstance()->utenteCorrente->cognome + "\",ente:\"" + getInstance()->utenteCorrente->ente + "\",ruolo:" + std::to_string(getInstance()->utenteCorrente->ruolo) + "}";
    (*reply)["user"] = userInfo;
}

void GNSShare::logout(std::string username, std::string password, std::map<std::string, std::string>* reply ){
    bool* resp = AuthService::logout(username, password, getInstance());
    (*reply)["isAuthenticated"] = *resp ? "0" : "1";
    delete resp; resp = nullptr;
};

void GNSShare::nuovoUtente(std::string username, std::string password, 
        const std::string usr, std::string pwd, std::string nome, std::string cognome, std::string ente, int ruolo){
    GNSShare* factory = AuthService::factoryGNSShare(username, password);
    try{
        factory-> _nuovoUtente(usr, pwd, nome, cognome, ente, ruolo);
    }
    catch(const std::exception &e){
        delete factory; factory = nullptr;
        throw e.what();
    }
    delete factory; factory = nullptr;
}

void GNSShare::nuovaStazioneGNSS(std::string username, std::string password, 
        const std::string codice, std::string nome, float lat, float lng, float alt, 
        std::string rete, std::string tipo, bool gps, bool gal, bool glo){
    
    GNSShare* factory = AuthService::factoryGNSShare(username, password);
    try{
        factory->_nuovaStazioneGNSS(codice, nome, lat, lng, alt, rete, tipo, gps, gal, glo);
    }
    catch(const std::exception &e){
        delete factory; factory = nullptr;
        throw e.what();
    }
    delete factory; factory = nullptr;
};

/*
std::string* GNSShare::elencoStazioniGNSS(std::string username, std::string password){
    GNSShare* factory = AuthService::factoryGNSShare(username, password);
    return factory->_elencoStazioniGNSS();
};
*/

void GNSShare::stampaUtenti(std::string username, std::string password){
    GNSShare* factory = AuthService::factoryGNSShare(username, password);
    factory->_stampaUtenti();
};

std::string* GNSShare::uploadFile(std::string username, std::string password, 
    std::string codiceStazione, std::string dataCreazione, float frequenza, std::string inizioPeriodo, std::string finePeriodo, std::string estensione){
    GNSShare* factory = AuthService::factoryGNSShare(username, password);
    std::string* nomedelfile;
    try{
        nomedelfile = factory->_uploadFile(codiceStazione, dataCreazione, frequenza, inizioPeriodo, finePeriodo, estensione);
    }
    catch(const std::exception &e){
        delete factory; factory = nullptr;
        throw e.what();
    }

    delete factory; factory = nullptr;
    return nomedelfile; //verificare se posso restituire un puntatore ad una stringa, se l'endpoint viene chiamato dall'esterno
};

void GNSShare::downloadFile(std::string username, std::string password, 
    std::string codiceStazione, std::string nome){
    GNSShare* factory = AuthService::factoryGNSShare(username, password);
    try{
        factory->_downloadFile(codiceStazione, nome);
    }
    catch(const std::exception &e){
        delete factory; factory = nullptr;
        throw e.what();
    }

    delete factory; factory = nullptr;
};

std::string* GNSShare::getStazioniGNSS(std::string username, std::string password, bool kv){
    GNSShare* factory = AuthService::factoryGNSShare(username, password);
    return factory->_getStazioniGNSS(kv);
};

std::vector<std::uint8_t>* GNSShare::sendRESTtoPython(std::string entrypoint, std::string method = "GET"){
    std::cout << "chiamata REST al modulo Python di tipo " << method << std::endl;
    std::cout << "-->>" << entrypoint << std::endl;
    
    http::Request request{entrypoint};

    const auto response = request.send(method);

    std::vector<std::uint8_t>* var = new std::vector<std::uint8_t>(response.body);
    std::cout << var->size() << " byte ricevuti da Python" << std::endl;

    return var;
};