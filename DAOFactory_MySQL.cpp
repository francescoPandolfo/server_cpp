//#include <iostream>

DAOFactory_MySQL::DAOFactory_MySQL(){
    //std::cout << "Chiamato il costruttore di DAOFactory_MySQL" << std::endl;
};

DAOFactory_MySQL::~DAOFactory_MySQL(){
    std::cout << "Chiamato il distruttore di DAOFactory_MySQL" << std::endl;
};

/*classe singleton*/
DAOFactory_MySQL* DAOFactory_MySQL::getInstance(){
    if ( instance == nullptr){
        instance = new DAOFactory_MySQL(); //creato dinamicamente
    }
    return instance;
};

/*--------------------- UTENTE     -----------------------------*/

void DAOFactory_MySQL::salvaUtente(Utente* ut){
    this->utente = ut;
    salvaUtente_DAO(); //metodo dell'"interfaccia" (superclasse)
};

persistence::DAO::Utente* DAOFactory_MySQL::factoryUtente(){
    std::cout <<"chiamata la factoryUtente della derivata DAOFactory_MySQL" << std::endl;
    int ruoloU = (int)this->utente->ruolo;
    persistence::DAO::Utente* toReturn = new persistence::DAO::MySQL::Utente(
        this->utente->username,
        this->utente->password,
        this->utente->nome,
        this->utente->cognome,
        this->utente->ente,
        ruoloU
    ); //la deallocazione della memoria viene chiamata esplicitamente dall'interfaccia che ha invocato questo metodo

    return toReturn;
};

std::map<std::string, Utente*>* DAOFactory_MySQL::caricaListaUtenti(){
    std::list<persistence::DAO::Utente*>* listaUt = persistence::DAO::MySQL::_OggettiDAO::estraiUtenti();  
    std::map<std::string, Utente*>* toReturn = new std::map<std::string, Utente*>();

     for(std::list<persistence::DAO::Utente*>::iterator it = listaUt->begin(); it != listaUt->end(); ++it){
         Utente::role ruoloUt = Utente::role((*it)->ruolo);

         Utente* ut = new Utente(
             (*it)->username, //ciò a cui punta il puntatore it è anch'esso un puntatore, quindi uso ->
             (*it)->password, 
             (*it)->nome,
             (*it)->cognome,
             (*it)->ente,
             ruoloUt
         );
    
        (*toReturn)[ut->username] = ut; 

        delete *it; //dato che non serve più, libero la parte di memoria dell'iesimo DAO ...
        *it = nullptr;
    }

    delete listaUt; //... infine cancello il puntatore ala lista di puntatori.
    listaUt = nullptr;

    return toReturn;
}

/*------------------ STAZIONE GNSS -----------------------------*/

void DAOFactory_MySQL::salvaStazioneGNSS(StazioneGNSS* st){
    this->stazioneGNSS = st;
    salvaStazioneGNSS_DAO(); //metodo dell'"interfaccia" (superclasse)
};

persistence::DAO::StazioneGNSS* DAOFactory_MySQL::factoryStazioneGNSS(){
    std::cout <<"chiamata la factoryStazioneGNSS della derivata DAOFactory_MySQL" << std::endl;
    persistence::DAO::StazioneGNSS *toReturn = new persistence::DAO::MySQL::StazioneGNSS(
        this->stazioneGNSS->codice,
        this->stazioneGNSS->nome,
        this->stazioneGNSS->latitudine,
        this->stazioneGNSS->longitudine,
        this->stazioneGNSS->altitudine,
        this->stazioneGNSS->rete,
        this->stazioneGNSS->tipo,
        this->stazioneGNSS->gps,
        this->stazioneGNSS->galileo,
        this->stazioneGNSS->glonass); //la deallocazione della memoria viene chiamata esplicitamente dall'interfaccia che ha invocato questo metodo

    return toReturn;
};

std::map<std::string, StazioneGNSS*>* DAOFactory_MySQL::caricaListaStazioniGNSS(){
    std::list<persistence::DAO::StazioneGNSS*>* listaStaz = persistence::DAO::MySQL::_OggettiDAO::estraiStazioniGNSS();  
    std::map<std::string, StazioneGNSS*>* toReturn = new std::map<std::string, StazioneGNSS*>();

     for(std::list<persistence::DAO::StazioneGNSS*>::iterator it = listaStaz->begin(); it != listaStaz->end(); ++it){
         StazioneGNSS* st = new StazioneGNSS(
             (*it)->codice, //ciò a cui punta il puntatore it è anch'esso un puntatore, quindi uso ->
             (*it)->nome, 
             (*it)->latitudine,
             (*it)->longitudine,
             (*it)->altitudine,
             (*it)->rete,
             (*it)->tipo,
             (*it)->gps,
             (*it)->galileo,
             (*it)->glonass
         );

         //carico la lista di file associati alla stazione
         std::map<std::string, File*>* momFiles = caricaListaFilePerStazione(st->codice);
         st->elencoFile = momFiles;
    
        (*toReturn)[st->codice] = st; 

        delete *it; //dato che non serve più, libero la parte di memoria dell'iesimo DAO ...
        *it = nullptr;
    }

    delete listaStaz; //... infine cancello il puntatore ala lista di puntatori.
    listaStaz = nullptr;

    return toReturn;
};

/*------------------ METADATI FILE -----------------------------*/
void DAOFactory_MySQL::salvaMetadatiFile(File* f){
    this->file = f;
    salvaMetadatiFile_DAO(); //metodo dell'interfaccia
};

void DAOFactory_MySQL::incrementaDownload(File* f){
    this->file = f;
    incrementaDownload_DAO(); //metodo dell'interfaccia
};

persistence::DAO::MetadatiFile* DAOFactory_MySQL::factoryMetadatiFile(){
    std::cout <<"chiamata la factoryMetadatiFile della derivata DAOFactory_MySQL" << std::endl;
    persistence::DAO::MetadatiFile *toReturn = new persistence::DAO::MySQL::MetadatiFile(
        this->file->codStazione, 
        this->file->nome, 
        this->file->dataCreazione, 
        this->file->frequenza,
        this->file->inizioPeriodo, 
        this->file->finePeriodo);

    return toReturn;
};

/*------------------ MOVIMENTI FILE -----------------------------*/
void DAOFactory_MySQL::salvaMovimentiFile(MovimentiFile* mf){
    this->movimentiFile = mf;
    salvaMovimentiFile_DAO(); //metodo dell'interfaccia
};

persistence::DAO::MovimentiFile* DAOFactory_MySQL::factoryMovimentiFile(){
    std::cout <<"chiamata la factoryMovimentiFile della derivata DAOFactory_MySQL" << std::endl;
    persistence::DAO::MovimentiFile *toReturn = new persistence::DAO::MySQL::MovimentiFile(
        this->movimentiFile->username, 
        this->movimentiFile->tipoMovimento, 
        this->movimentiFile->nomeFile);

    return toReturn;
};

/*--------------------------------------------------------------*/

std::map<std::string, File*>* DAOFactory_MySQL::caricaListaFilePerStazione(std::string codiceStazione){
    std::list<persistence::DAO::MetadatiFile*>* listDAO = persistence::DAO::MySQL::_OggettiDAO::estraiFilesPerStazione(codiceStazione);
    std::map<std::string, File*>* toReturn = new std::map<std::string, File*>();

    for(std::list<persistence::DAO::MetadatiFile*>::iterator it = listDAO->begin(); it != listDAO->end(); ++it){
         File* f = new File(
             (*it)->codStazione, //ciò a cui punta il puntatore it è anch'esso un puntatore, quindi uso ->
             (*it)->nome, 
             (*it)->dataCreazione,
             (*it)->frequenza,
             (*it)->inizioPeriodo,
             (*it)->finePeriodo
         );
    
        (*toReturn)[f->nome] = f; 

        delete *it; //dato che non serve più, libero la parte di memoria dell'iesimo DAO ...
        *it = nullptr;
    }

    delete listDAO; //... infine cancello il puntatore ala lista di puntatori.
    listDAO = nullptr;

    return toReturn;
};

