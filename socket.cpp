//https://www.geeksforgeeks.org/socket-programming-cc/

// Server side C/C++ program to demonstrate Socket programming
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <thread>

#define PORT 8080

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>
#include <exception>

#include "classi.h"
//#include "utility.cpp"
#include "GNSShare.cpp"

struct MessageReceived{
	std::string username, password;
	int operazione;
	long ipClient;
	
	std::map<std::string, std::string> parametri;

	std::string toString(){ 
		return "address: " + std::to_string(ipClient) + " username: " + username + " password: " + password + " operazione: " + GNSShare::menu_names.at(operazione);
	};
		    
};

GNSShare* server;
bool activeNewSocket = true;
std::string URL_PYTHON_MODULE = "http://127.0.0.1:5000";
const int DIM_BUFFER_READ = 1000000; //2048000;

std::map<std::string, std::string>* _deserializeJson(std::string s){
	//std::cout << "PARAMETRI: " << s << std::endl;
	std::map<std::string, std::string>* toReturn = new std::map<std::string, std::string>();

	std::string del_elem = ","; //delimita gli elementi che formano il Json
	std::string del_kv = ":"; //delimita le chiavi/valore di un singolo elemento

	size_t pos_elem = 0;
	size_t pos_kv = 0;
	std::string token_elem;
	std::string token_k;
	std::string token_v;

	s = s.substr(1, s.length()-2);
	//std::cout << s << std::endl;
	pos_elem = s.find(del_elem);
	token_elem = s.substr(0, pos_elem);
	while(true){
	//while ((pos_elem = s.find(del_elem)) != std::string::npos) {
		
		pos_kv = token_elem.find(del_kv);
		token_k = token_elem.substr(1,pos_kv-2);
		token_v = token_elem.substr(pos_kv + del_kv.length(), token_elem.length() - pos_kv - del_kv.length());

		if(token_v.substr(0,1) == "\"") token_v = token_v.substr(1, token_v.length() - 2); //elimina le " "

		(*toReturn)[token_k] = token_v;
		
		//std::cout << " -> " << token_elem << std::endl;
		//std::cout << token_k << " " << token_v << std::endl;

		if(pos_elem != std::string::npos) s.erase(0, pos_elem + del_elem.length());
		else break;

		pos_elem = s.find(del_elem);

		if(pos_elem != std::string::npos) token_elem = s.substr(0, pos_elem);
		else token_elem = s;
	}
	return toReturn;
};

MessageReceived* deserializeJson(std::string s){
	MessageReceived* toReturn = new MessageReceived();

	std::string del_elem = ","; //delimita gli elementi che formano il Json
	std::string del_kv = ":"; //delimita le chiavi/valore di un singolo elemento

	size_t pos_elem = 0;
	size_t pos_kv = 0;
	std::string token_elem;
	std::string token_k;
	std::string token_v;

	s = s.substr(1, s.length()-2);
	//std::cout << s << std::endl;
	pos_kv = s.find(del_kv); //se il Value inizia con "{" allora l'elemento finirà alla chiusura "}"
	std::string carattereSuccessivo = s.substr(pos_kv+1,1);
	//std::cout << "carattereSuccessivo " << carattereSuccessivo << std::endl;
	if(carattereSuccessivo == "{"){
		size_t pos_chiusura = s.find("}");
		pos_elem = s.substr(pos_chiusura,s.length()-pos_chiusura).find(del_elem);
	}
	else pos_elem = s.find(del_elem); 

	token_elem = s.substr(0, pos_elem);
	while(true){
		//std::cout << "TOKEN ELEM: " << token_elem << std::endl;
		
		pos_kv = token_elem.find(del_kv);
		token_k = token_elem.substr(1,pos_kv-2);
		token_v = token_elem.substr(pos_kv + del_kv.length(), token_elem.length() - pos_kv - del_kv.length());

		if(token_v.substr(0,1) == "\"") token_v = token_v.substr(1, token_v.length() - 2); //elimina le " "

		//std::cout << token_k << " " << token_v << std::endl;

		if(token_k == "ipClientAddress") toReturn->ipClient = std::stol(token_v);
		else if(token_k == "username") toReturn->username = token_v;
		else if(token_k == "password") toReturn->password = token_v;
		else if(token_k == "operazione") toReturn->operazione = std::stoi(token_v);
		else if(token_k == "parametri") toReturn->parametri = *_deserializeJson(token_v);
		//std::cout << " -> " << token_elem << std::endl;
		//std::cout << token_k << " " << token_v << std::endl;

		if(pos_elem != std::string::npos) s.erase(0, pos_elem + del_elem.length());
		else break;

		pos_kv = s.find(del_kv); //se il Value inizia con "{" allora l'elemento finirà alla chiusura "}"
		carattereSuccessivo = s.substr(pos_kv+1,1);
		//std::cout << "carattereSuccessivo " << carattereSuccessivo << std::endl;
		if(carattereSuccessivo == "{"){
			size_t pos_chiusura = s.find("}");
			pos_elem = s.substr(pos_chiusura,s.length()-pos_chiusura).find(del_elem);
		}
		else pos_elem = s.find(del_elem);

		//Potrebbero essere aggiunti caratteri indesiderati a fine stringa
		if(carattereSuccessivo == "{"){
			//assicurarsi che i parametri stiano sempre in coda
			size_t pos_chiusura = s.find("}");
			s.erase(pos_chiusura + 1, s.length() - pos_chiusura);
			token_elem = s;
		}
		else{
			if(pos_elem != std::string::npos) token_elem = s.substr(0, pos_elem);
			else token_elem = s;
		}
	}
	return toReturn;
};

std::string* serializeJson(std::map<std::string, std::string>* message){
	std::string toReturn = "{";
	
	if(message != nullptr && message->size() > 0){
		for (std::map<std::string, std::string>::iterator it = message->begin(); it != message->end(); ++it){
			toReturn = toReturn + (*it).first + ":" + (*it).second + ","; 
		}
		toReturn.pop_back();
	}
	else toReturn += "operationCompleted:0,serverReply:\"Errore generico: il server non ha prodotto nessuna risposta\"";
	
	
	toReturn += "}";
	
	std::string* _toReturn = new std::string(toReturn);
	return _toReturn;
}

void startNewSocket(){

	while(true){

		int server_fd, new_socket, valread;
		struct sockaddr_in address;
		int opt = 1;
		int addrlen;// = sizeof(address);
		

		opt = 1;
		addrlen = 16;

		std::cout << utility::textColor_green("socket in ascolto...") << std::endl;
		// Creating socket file descriptor
		if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
			perror("socket failed");
			exit(EXIT_FAILURE);
		}
		
		// Forcefully attaching socket to the port 8080
		if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
			perror("setsockopt");
			exit(EXIT_FAILURE);
		}
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = INADDR_ANY;
		address.sin_port = htons( PORT );
		
		// Forcefully attaching socket to the port 8080
		if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
			perror("bind failed");
			exit(EXIT_FAILURE);
		}
		if (listen(server_fd, 3) < 0){
			perror("listen");
			exit(EXIT_FAILURE);
		}
		if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0){
			perror("accept");
			exit(EXIT_FAILURE);
		}

		bool continueReceaving = false;
		std::string* totalMessage = new std::string("");
		std::map<std::string, std::string>* reply = new std::map<std::string, std::string>();
		bool replyAlreadySent = false;
		std::string* replyMessage;
		
		while(true){
			char buffer[DIM_BUFFER_READ] = {0};			
			
			valread = read( new_socket , buffer, DIM_BUFFER_READ);
			//std::cout << totalMessage->size() << std::endl;
			//printf("%s\n",buffer);
			//std::cout << "valread " << valread << std::endl;
			if(valread <= 0) {
				std::cout << utility::textColor_red("Collegamento col client interrotto!") << std::endl;
				sleep(2);
				break;
			}
			else{
				if(continueReceaving){
					if(std::string(buffer) == "#FINE-FILE#"){
						std::cout << "Ricevuto file di " << totalMessage->size() << " bytes" << std::endl;
						//si è conclusa la ricezione di più pacchetti (un nuovo file)
						continueReceaving = false;

						//salvo il file
						std::string* codiceStaz = new std::string( (*reply)["codiceStazione"] );
						codiceStaz->pop_back(); codiceStaz->erase(0,1);
						std::string* nomeF = new std::string((*reply)["nomeFile"]);
						nomeF->pop_back(); nomeF->erase(0,1);
						
						std::string* pathToSave = new std::string("REPOSITORY/" + *codiceStaz + "/" + *nomeF);

						std::ofstream nuovoFile( (*pathToSave) );
						if(nuovoFile.is_open()){
							nuovoFile << *totalMessage;
							nuovoFile.close();
							std::cout << "File salvato in: " + *pathToSave << std::endl;
						}
						else{
							std::cout << "FILE NON SALVATO" << std::endl;
						}
						
						delete pathToSave; pathToSave = nullptr;
						delete codiceStaz; codiceStaz = nullptr;
						delete nomeF; nomeF = nullptr;
						delete totalMessage;
						totalMessage = new std::string("");
					}
					else *totalMessage += std::string(buffer);
				}
				else{
					delete totalMessage;
					totalMessage = new std::string("");

					std::cout << "<<-- " << buffer << std::endl;
					//converto il Json ricevuto in un oggetto
					MessageReceived* mr = deserializeJson(buffer);
					std::cout << "<<-- " << utility::textColor_yellow(mr->toString()) << std::endl;

					reply = new std::map<std::string, std::string>();
					std::vector<std::uint8_t>* replyREST;
					replyMessage = new std::string("");
					(*reply)["ipClientAddress"] = std::to_string(mr->ipClient);
					replyAlreadySent = false;
					std::string* listaParametri = new std::string();

					try{
						switch(mr->operazione){
							case GNSShare::menu::LOGIN : 
								server->login(mr->username, mr->password, reply);
								break;
							case GNSShare::menu::LOGOUT : 
								server->logout(mr->username, mr->password, reply);
								break;
							case GNSShare::menu::AGGIUNGI_UTENTE :
								server->nuovoUtente(mr->username, mr->password, (mr->parametri)["username"], 
									(mr->parametri)["password"], (mr->parametri)["nome"], (mr->parametri)["cognome"], (mr->parametri)["ente"], std::stoi( (mr->parametri)["ruolo"]));
								(*reply)["operationCompleted"] = "1";
								break;
							case GNSShare::menu::AGGIUNGI_STAZIONE :
								server->nuovaStazioneGNSS(mr->username, mr->password,
									(mr->parametri)["codice"], (mr->parametri)["nome"], std::stof((mr->parametri)["latitudine"]), std::stof((mr->parametri)["longitudine"]), std::stof((mr->parametri)["altitudine"]), 
									(mr->parametri)["rete"], (mr->parametri)["tipo"], 
									(mr->parametri)["gps"] == "True", (mr->parametri)["galileo"] == "True", (mr->parametri)["glonass"] == "True");
									(*reply)["operationCompleted"] = "1";
								break;
							case GNSShare::menu::CARICA_FILE :
								(*reply)["nomeFile"] = "\"" + *(server->uploadFile(mr->username, mr->password,
									(mr->parametri)["codiceStazione"], (mr->parametri)["dataCreazione"], 
									std::stof((mr->parametri)["frequenza"]), (mr->parametri)["inizioPeriodo"], (mr->parametri)["finePeriodo"], (mr->parametri)["estensione"])) + "\"";

								(*reply)["operationCompleted"] = "1";

								//purtroppo dopo svariate prove di far scrivere lo stream di byte ricevuto, commento le righe che seguono in modo da non attendersi nessun successivo invio
								/*(*reply)["codiceStazione"] = "\"" + (mr->parametri)["codiceStazione"] + "\"";
								continueReceaving = true;
								std::cout << "Ricezione file ....." << std::endl;*/
								break;
							case GNSShare::menu::DOWNLOAD_FILE :
								server->downloadFile( mr->username, mr->password, (mr->parametri)["codiceStazione"], (mr->parametri)["nomeFile"] );
								(*reply)["operationCompleted"] = "1";
								break;
							case GNSShare::menu::GET_STAZIONI :
								(*reply)["operationCompleted"] = "1";
								(*reply)["elencoStazioni"] = *(server->getStazioniGNSS( mr->username, mr->password, (mr->parametri)["kv"] == "1"?true:false ));
								break;
							case GNSShare::menu::GET_MAPPA_STAZIONI :
								for (std::map<std::string, std::string>::iterator it = (mr->parametri).begin(); it != (mr->parametri).end(); ++it)
									*listaParametri += it->first + "=" + it->second + "&";
								if((mr->parametri).count("codiceStazione"))
									replyREST = server->sendRESTtoPython(URL_PYTHON_MODULE + "/plots/stationmap?" + *listaParametri);
								else replyREST = server->sendRESTtoPython(URL_PYTHON_MODULE + "/plots/europemap?" + *listaParametri);
								std::cout << "-->> " << replyREST->size() << " byte inviati al client" << std::endl;

								send(new_socket , (*replyREST).data(), replyREST->size() , 0 );
								delete replyREST; replyREST = nullptr;
								replyAlreadySent = true;
								break;
								
							case GNSShare::menu::REFRESH_MAPPA_STAZIONI :
								replyREST = server->sendRESTtoPython(URL_PYTHON_MODULE + "/plots/refresheuropemap");
								(*reply)["operationCompleted"] = "1";
								delete replyREST; replyREST = nullptr;
								break;

							case GNSShare::menu::STATISTICHE :
								for (std::map<std::string, std::string>::iterator it = (mr->parametri).begin(); it != (mr->parametri).end(); ++it)
									*listaParametri += it->first + "=" + it->second + "&";
								replyREST = server->sendRESTtoPython(URL_PYTHON_MODULE + "/plots/plotstat?" + *listaParametri);
								std::cout << "<<-- " << replyREST->size() << " byte inviati al client" << std::endl;

								send(new_socket , (*replyREST).data(), replyREST->size() , 0 );
								delete replyREST; replyREST = nullptr;
								replyAlreadySent = true;
								break;
							default:
								throw new std::string("Operazione non prevista.");
						}
					}
					catch(std::string* str){
						std::list<char> car = {'\"', '{', '}'};
						utility::eliminaCaratteriSpeciali(str, car);
						//reply deve avere la stessa struttura della struct "objectResponseGeneric" del client che invia la richiesta
						std::cout << "********gestione errore: std::string* str" << std::endl;
						(*reply)["operationCompleted"] = "0";
						(*reply)["serverReply"] = "\"" + *str + "\"";
						//delete str; str = nullptr;
					}
					catch(char const* str){
						std::cout << "********gestione errore: char const* str" << std::endl;
						(*reply)["operationCompleted"] = "0";
						(*reply)["serverReply"] = "\"" + std::string(str) + "\""; 
					}
					catch(std::invalid_argument ex){
						(*reply)["operationCompleted"] = "0";
						(*reply)["serverReply"] = "\"Invalid argument.\"";
					}
					catch(std::out_of_range ex){
						(*reply)["operationCompleted"] = "0";
						(*reply)["serverReply"] = "\"Out of range: " + std::string(ex.what()) + "\"";
					}
					catch(...){
						std::exception_ptr p = std::current_exception();
						(*reply)["operationCompleted"] = "0";
						(*reply)["serverReply"] = "\"" + std::string((p ? p.__cxa_exception_type()->name() : "null")) + "\"";
					}

					delete listaParametri; listaParametri = nullptr;
					delete mr; mr = nullptr;
				}

				if (!continueReceaving){//concludo la comunicazione solo se non c'è altro da ricevere (il file)
					if(!replyAlreadySent){
						replyAlreadySent = false;
						replyMessage = serializeJson(reply);
						
						std::cout << "-->> " << *replyMessage << std::endl;
						delete reply;
						send(new_socket , (*replyMessage).c_str() , strlen((*replyMessage).c_str()) , 0 );
					}				
					
					delete replyMessage; //pronto per il prossim reply al client
					sleep(1);
				}
			}
		}
	}	
}

int main(int argc, char const *argv[]){
	std::cout << utility::textColor_green("Inizio programma GNSShare.") << std::endl;
	server = GNSShare::getInstance();
	server->_avviamento();

	/*
	while(true){
		
		if(activeNewSocket){
			//std::thread thread_obj(startNewSocket);
			//thread_obj.join();
			int pthread_detach(pthread_t startNewSocket);
			activeNewSocket = false;
		}
		sleep(1);
		std::cout << "giro" << std::endl;
	}*/
	
	startNewSocket();
	
}
