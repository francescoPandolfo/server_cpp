/*
Le classi derivate di GNSShare devono inibire (tramite override) l'accesso alle funzioni in base al ruolo dell'utente
Tali funzioni sono dichiarate nella classe base come "virtual" così che se una derivata non la sovrascrive viene chiamat la funzione base
Con questa tecnica si possono creare anche gerarchie di ruoli;
*/

class GNSShare_NOT_LOGGED : public GNSShare{
public:
	GNSShare_NOT_LOGGED(){
    //    std::cout << "Chiamato il costruttore di GNSShare_NOT_LOGGED" << std::endl;
    };

	void _nuovaStazioneGNSS() override{ utility::accessoNegato(); };
    void _nuovaStazioneGNSS(const std::string&, std::string&, float&, float&, float&, std::string&, std::string&, bool&, bool&, bool&) override{ utility::accessoNegato(); };
    void _nuovoUtente() override{ utility::accessoNegato(); };
    void _nuovoUtente(const std::string&, std::string&, std::string&, std::string&, std::string&, int&) override{ utility::accessoNegato(); };
    void _uploadFile() override{ utility::accessoNegato(); };
    void _downloadFile(std::string&, std::string&)  override{ utility::accessoNegato(); throw "Funzione non disponibile";};
    std::string* _uploadFile(/*File, */std::string&, std::string&, float&, std::string&, std::string&, std::string&) override{ utility::accessoNegato(); };
    void _stampaUtenti() override{ utility::accessoNegato(); };
    //std::string* _elencoStazioniGNSS() override{ utility::accessoNegato(); };
};

class GNSShare_LOGGED : public GNSShare{
protected: //non devono essere create istanze di questo ruolo generico, ma solo dei suoi derivati
	GNSShare_LOGGED(){};
public:
    void _login() override{ utility::accessoNegato(); };
	void _login(std::string&, std::string&) override{ utility::accessoNegato(); };
};

class GNSShare_ADMIN : public GNSShare_LOGGED{
public:
	GNSShare_ADMIN(){};
    /*L'utente amministratore può fare tutto, quindi non inibisce nessun metodo*/
};

class GNSShare_EDITOR : public GNSShare_LOGGED{
public:
	GNSShare_EDITOR(){};
    /*L'editor può caricare e scaricare file, non può inserire nuove stazioni*/
    void _nuovaStazioneGNSS() override{ utility::accessoNegato(); };
    void _nuovaStazioneGNSS(const std::string&, std::string&, float&, float&, float&, std::string&, std::string&, bool&, bool&, bool&) override{ utility::accessoNegato(); };
    void _nuovoUtente() override{ utility::accessoNegato(); };
    void _nuovoUtente(const std::string&, std::string&, std::string&, std::string&, std::string&, int&) override{ utility::accessoNegato(); };
    void _stampaUtenti() override{ utility::accessoNegato(); };
};

class GNSShare_VIEWER : public GNSShare_EDITOR{
public:
	GNSShare_VIEWER(){};
    /*Come l'editor ma non può caricare file, ma solo scaricarli*/
    void _uploadFile() override{ utility::accessoNegato(); };
    std::string* _uploadFile(std::string&, std::string&, float&, std::string&, std::string&, std::string&) override{ utility::accessoNegato(); };
};

/*********** AUTENTICAZIONE *************/
/*
La classe AuthService ha il compito di creare dinamicamente e restituire al chiamante la derivata di GNSShare relativa al ruolo dell'utente;
la deallocazione della memoria spetta al chiamante che riceve il puntatore all'istanza della classe derivata.
Non occorre creare un'istanza di AuthService quindi tutti i metodi sono dichiarati statici.
*/

class AuthService{
public:
    static std::map<std::string, Utente*>* elencoUtenti;

    /*valori di ritorno: 0 OK, 1 Username non valido, 2 Password non valida, 3 errore generico*/
    static void login(const std::string username, std::string password, GNSShare *chiamante){    
        if ( AuthService::elencoUtenti->find(username) == AuthService::elencoUtenti->end() ){
            chiamante->utenteCorrente = nullptr;
            std::cout << utility::textColor_red("Utente non trovato") << std::endl;
            throw "Utente non trovato";
        }
        else{
            if (password != AuthService::elencoUtenti->at(username)->password){
                chiamante->utenteCorrente = nullptr;
                std::cout << utility::textColor_red("Password errata!") << std::endl;
                throw "Password errata!";
            }
            else {
                chiamante->utenteCorrente = AuthService::elencoUtenti->at(username);
                std::cout << utility::textColor_green("Login per l'utente " + username + " avvenuto con successo!") << std::endl;
            }
        }
    }

    static bool* logout(const std::string username, std::string password, GNSShare *chiamante){
        bool* toReturn = new bool(true);                                
        chiamante->utenteCorrente = nullptr;

        std::cout << utility::textColor_green("Logout per l'utente " + username + " avvenuto con successo!") << std::endl;
        return toReturn;
    }

    static GNSShare* factoryGNSShare(){
        return new GNSShare_NOT_LOGGED();
    };

    static GNSShare* factoryGNSShare(std::string username, std::string password){
        Utente *ut;
        if ( AuthService::elencoUtenti->find(username) == AuthService::elencoUtenti->end() ){
            std::cout << utility::textColor_red("\nUtente non trovato!\n");
        }
        else{
            ut = AuthService::elencoUtenti->at(username);
            if(ut->password != password){
                std::cout << utility::textColor_red("\nPassword errata per l'utente " + username + "\n");
                ut = nullptr;
            }
        }

		if (ut == nullptr) return new GNSShare_NOT_LOGGED();

		switch(ut->ruolo){
			case Utente::role::NOT_LOGGED : return new GNSShare_NOT_LOGGED(); break;
            case Utente::role::ADMIN : return new GNSShare_ADMIN(); break;
            case Utente::role::EDITOR : return new GNSShare_EDITOR(); break;
            case Utente::role::VIEWER : return new GNSShare_VIEWER(); break;
			default: return new GNSShare_NOT_LOGGED();
		}
    };
};

//std::map<std::string, Utente*>* AuthService::elencoUtenti = { { "", new Utente() } };
std::map<std::string, Utente*>* AuthService::elencoUtenti = nullptr;
//delete AuthService::elencoUtenti[""];