//#include <iostream>

//costruttori
Stazione::Stazione(){ this->elencoFile = nullptr; };

Stazione::Stazione(const std::string codice, std::string nome, float lat, float lng, float alt){
	//std::cout << "Chiamato costruttore Stazione(x5)" << std::endl;
	this->codice 		= codice;
	this->nome 			= nome;
	this->latitudine 	= lat;
	this->longitudine 	= lng;
	this->altitudine 	= alt;

	this->elencoFile = new std::map<std::string, File*>();
};

//distruttori
Stazione::~Stazione(){};

//metodi
void Stazione::stampa(){
	std::cout << "[Stazione] " << this->to_string() << std::endl;
};

std::string Stazione::to_string() const{
	return utility::textColor_green(this->codice + " (" + this->nome + ") ") + std::to_string(this->latitudine) + " " + std::to_string(this->longitudine) + " " + std::to_string(this->altitudine);
};

std::string* Stazione::uploadFile(std::string& dataCreazione, float& frequenza, std::string& inizioPeriodo, std::string& finePeriodo, std::string& estensione){
	std::string nomeFile = this->codice + "_" + inizioPeriodo + "_" + finePeriodo +"_f" + std::to_string(frequenza) + "." + estensione;
	std::list<char> car = {':'};
	utility::eliminaCaratteriSpeciali(&nomeFile, car);

	File* fileCorrente = new File(this->codice, nomeFile, dataCreazione, frequenza, inizioPeriodo, finePeriodo);

	std::cout << "CHIAMATO uploadFile" << std::endl;

	fileCorrente->storeDatabase();
	std::cout << utility::textColor_green("** Metadati del file \"" + nomeFile + "\" salvati nel database!") << std::endl;
	
	(this->elencoFile)->insert( { nomeFile, fileCorrente }); //altro modo per aggiungere elementi alla mappa
	std::cout << utility::textColor_green("** File \"" + nomeFile + "\" aggiunto a sistema, alla stazione " + this->codice) << std::endl;
	//std::cout << ((this->elencoFile)->at(nomeFile))->nome << "\t\t" << std::endl;

	return &(((this->elencoFile)->at(nomeFile))->nome);
};

void Stazione::downloadFile(std::string nomeFile){
	File* fileCorrente = elencoFile->at(nomeFile);
	fileCorrente->incrementaDownload();
};
