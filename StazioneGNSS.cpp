//#include <iostream>

StazioneGNSS::StazioneGNSS() : Stazione(){
	std::cout << "Chiamato il costruttore di StazioneGNSS()" << std::endl;
};

StazioneGNSS::StazioneGNSS(const std::string codice, std::string nome,
			float lat, float lng, float alt,
			std::string rete, std::string tipo,
			bool gps, bool galileo, bool glonass) : Stazione(codice, nome, lat, lng, alt){
	//std::cout << "Chiamato il costruttore di StazioneGNSS(x10)" << std::endl;

	this->rete 		= rete;
	this->tipo 		= tipo;
	this->gps 		= gps;
	this->galileo 	= galileo;
	this->glonass 	= glonass;

	//std::cout << "Creata stazione GNSS " << to_string() << std::endl;
};

StazioneGNSS::~StazioneGNSS(){
	std::cout << "Chiamato il distruttore di StazioneGNSS" << std::endl;
};

void StazioneGNSS::stampa(){
	std::cout << "[Stazione GNSS] " << to_string() << std::endl;
	std::cout << "Elenco file:\n";
	utility::stampaChiavi_PuntatoreMappa( this->elencoFile );
	std::cout << std::endl;
};

std::string StazioneGNSS::to_string() const{
	return Stazione::to_string() + "\nrete:" + rete + "\ntipo:" + tipo + "\ngps:" + std::to_string(gps) + "  gal:" + std::to_string(galileo) + "  glo:" + std::to_string(glonass); 
};

//kv se true popola solo la chiave e la descrizione
std::string* StazioneGNSS::to_JSON(bool kv) const{
	if(kv) return new std::string("{codice:\""+codice+"\",nome:\""+nome+"\"}");
	else return new std::string("{codice:\""+codice+"\",nome:\""+nome+"\",latitudine:"+std::to_string(latitudine)+",longitudine:"+std::to_string(longitudine)+",altitudine:"+std::to_string(altitudine)+",rete:\""+rete+"\",tipo:\""+tipo+"\",gps:" + std::to_string(gps)+",galileo:"+std::to_string(galileo)+",glonass:"+std::to_string(glonass)+",fileCaricati:"+ *(utility::PuntatoreMappaDiOggetti_toJSON(this->elencoFile)) +"}");
};

void StazioneGNSS::storeDatabase(){
	DAOFactory_MySQL* factory = DAOFactory_MySQL::getInstance();
	factory->salvaStazioneGNSS(this);
}