namespace persistence::DAO::MySQL{

    StazioneGNSS::StazioneGNSS(){
        std::cout <<"chiamato il costruttore di DAO::MySQL::StazioneGNSS" << std::endl;
    }

    StazioneGNSS::~StazioneGNSS(){
        std::cout <<"chiamato il distruttore di DAO::MySQL::StazioneGNSS" << std::endl;
    }

    void StazioneGNSS::save(){ //metodo ereditato dalla superclasse DAO
        
        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();
        MYSQL *con = MySQL_conn->getNewConnection();

        std::string myQ = "INSERT INTO StazioneGNSS(codice, nome, latitudine, longitudine, altitudine, rete, tipo, gps, galileo, glonass) VALUES("
            "'" + this->codice + "', " +
            "'" + this->nome + "', " +
            std::to_string(this->latitudine) + ", " +
            std::to_string(this->longitudine) + ", " +
            std::to_string(this->altitudine) + ", " +
            "'" + this->rete + "', " +
            "'" + this->tipo + "', " +
            std::to_string(this->gps) + ", " +
            std::to_string(this->galileo) + ", " +
            std::to_string(this->glonass) + ");";

        //std::cout << myQ << std::endl;    //da salvare nel file di log

        const char *myQuery = myQ.c_str(); //devo convertire string --> char*
        MySQL_conn->executeQuery(con, myQuery);
        

        std::cout <<"chiamato il metodo save() di DAO::MySQL::StazioneGNSS, per : " << this->codice << " (" << this->nome << ")" << std::endl;

        MySQL_conn->closeConnection(con, nullptr);
    };

}