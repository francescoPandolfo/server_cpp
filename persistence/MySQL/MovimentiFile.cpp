namespace persistence::DAO::MySQL{

    MovimentiFile::MovimentiFile(){
        std::cout <<"chiamato il costruttore di MovimentiFile_DAO_MySQL" << std::endl;
    }

    MovimentiFile::~MovimentiFile(){
        std::cout <<"chiamato il distruttore di MovimentiFile_DAO_MySQL" << std::endl;
    }

    void MovimentiFile::save(){ //metodo ereditato dalla superclasse DAO
        std::cout <<"chiamato il metodo save() di MovimentiFile_DAO_MySQL" << std::endl;

        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();
        MYSQL *con = MySQL_conn->getNewConnection();

        std::string myQ = "INSERT INTO MovimentiFile(username, tipoMovimento, nomeFile) VALUES("
            "'" + this->username + "', " +
            "'" + this->tipoMovimento + "', " +
            "'" + this->nomeFile + "');";

        std::cout << myQ << std::endl;    //da salvare nel file di log

        const char *myQuery = myQ.c_str(); //devo convertire string --> char*
        MySQL_conn->executeQuery(con, myQuery);
        

        std::cout <<"chiamato il metodo save() di DAO::MySQL::MovimentiFile" << std::endl;

        MySQL_conn->closeConnection(con, nullptr);
    };
}
