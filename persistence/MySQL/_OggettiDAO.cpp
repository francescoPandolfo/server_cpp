#include <cstring>

namespace persistence::DAO::MySQL{

    //Estrae l'elenco dei campi di una tabella in modo da permettere all'utilizzatore di accedere alle colonne di un rowset non per indice ma per nome_colonna
    std::map<std::string, int>* _OggettiDAO::estraiCampiTabella(const std::string &nomeTabella){
        std::map<std::string, int>* toReturn = new std::map<std::string, int>();

        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();
        std::string myQ = "SELECT * FROM " + nomeTabella + " LIMIT 1;";
        const char *myQuery = myQ.c_str(); //devo convertire string --> char*

        MYSQL *con = MySQL_conn->getNewConnection();
        mysql_query(con, myQuery);

        MYSQL_RES *result = mysql_store_result(con);
        MYSQL_FIELD *field;
        
        for(int i = 0; (field = mysql_fetch_field(result)); i++) (*toReturn)[field->name] = i;
        
        MySQL_conn->closeConnection(con, result);

        return toReturn;
    };

    std::list<persistence::DAO::Utente*>* _OggettiDAO::estraiUtenti(){
        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();  
        MYSQL *con = MySQL_conn->getNewConnection();
        MYSQL_RES *result;
        MYSQL_ROW row;  

        std::list<persistence::DAO::Utente*>* toReturn = new std::list<persistence::DAO::Utente*>();

        result = MySQL_conn->getQueryResult(con, result, "SELECT * from Utente;");

        std::map<std::string, int>* elencoCampi = persistence::DAO::MySQL::_OggettiDAO::estraiCampiTabella("Utente");
        
        while( (row = mysql_fetch_row(result)) != NULL ){
            persistence::DAO::Utente* ut = new persistence::DAO::Utente(
                    std::string(row[(*elencoCampi)["username"]]), 
                    std::string(row[(*elencoCampi)["password"]]),
                    std::string(row[(*elencoCampi)["nome"]]), 
                    std::string(row[(*elencoCampi)["cognome"]]),
                    std::string(row[(*elencoCampi)["ente"]]),
                    atoi(row[(*elencoCampi)["ruolo"]])
                    );
            toReturn->push_back( ut );
        }

        MySQL_conn->closeConnection(con, result);

        return toReturn;
    };


    std::list<persistence::DAO::StazioneGNSS*>* _OggettiDAO::estraiStazioniGNSS(){
        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();  
        MYSQL *con = MySQL_conn->getNewConnection();
        MYSQL_RES *result;
        MYSQL_ROW row;  

        std::list<persistence::DAO::StazioneGNSS*>* toReturn = new std::list<persistence::DAO::StazioneGNSS*>();

        result = MySQL_conn->getQueryResult(con, result, "SELECT * from StazioneGNSS ORDER BY codice;");

        std::map<std::string, int>* elencoCampi = persistence::DAO::MySQL::_OggettiDAO::estraiCampiTabella("StazioneGNSS");
        
        while( (row = mysql_fetch_row(result)) != NULL ){
            persistence::DAO::StazioneGNSS* stGNSS = new persistence::DAO::StazioneGNSS(
                    std::string(row[(*elencoCampi)["codice"]]), 
                    std::string(row[(*elencoCampi)["nome"]]),
                    std::stof(row[(*elencoCampi)["latitudine"]]),   //stof per il casting a float
                    std::stof(row[(*elencoCampi)["longitudine"]]),
                    std::stof(row[(*elencoCampi)["altitudine"]]),
                    std::string(row[(*elencoCampi)["rete"]]),
                    std::string(row[(*elencoCampi)["tipo"]]),
                    bool(row[(*elencoCampi)["gps"]]),
                    bool(row[(*elencoCampi)["galileo"]]),
                    bool(row[(*elencoCampi)["glonass"]])
                    );
            toReturn->push_back( stGNSS );
        }

        MySQL_conn->closeConnection(con, result);

        return toReturn;
    };

    std::list<persistence::DAO::MetadatiFile*>* _OggettiDAO::estraiFilesPerStazione(const std::string &codiceStazione){
        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();  
        MYSQL *con = MySQL_conn->getNewConnection();
        MYSQL_RES *result;
        MYSQL_ROW row;  

        std::list<persistence::DAO::MetadatiFile*>* toReturn = new std::list<persistence::DAO::MetadatiFile*>();

        std::string myQ = "SELECT * from MetadatiFile WHERE codiceStazione = '" + codiceStazione + "';";
        const char* myQuery = myQ.c_str(); //devo convertire string --> char*

        result = MySQL_conn->getQueryResult(con, result, myQuery);

        std::map<std::string, int>* elencoCampi = persistence::DAO::MySQL::_OggettiDAO::estraiCampiTabella("MetadatiFile");
        
        while( (row = mysql_fetch_row(result)) != NULL ){
            persistence::DAO::MetadatiFile* metFile = new persistence::DAO::MetadatiFile(
                    std::string(row[(*elencoCampi)["codiceStazione"]]), 
                    std::string(row[(*elencoCampi)["nome"]]),
                    std::string(row[(*elencoCampi)["dataCreazione"]]),   
                    std::stof(row[(*elencoCampi)["frequenza"]]),  //stof per il casting a float
                    std::string(row[(*elencoCampi)["inizioPeriodo"]]),
                    std::string(row[(*elencoCampi)["finePeriodo"]])
                    );
            toReturn->push_back( metFile );
        }

        MySQL_conn->closeConnection(con, result);

        return toReturn;
    };

}
