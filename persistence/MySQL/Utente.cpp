namespace persistence::DAO::MySQL{

    void Utente::save(){ //metodo ereditato dalla superclasse DAO
        std::cout <<"chiamato il metodo save() di DAO::MySQL::Utente, per : " << this->username << " (" << this->nome << " " << this->cognome << ")" << std::endl;
        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();
        MYSQL *con = MySQL_conn->getNewConnection();
        
        std::string myQ = "INSERT INTO Utente(username, password, nome, cognome, ente, ruolo) VALUES("
            "'" + this->username + "', " +
            "'" + this->password + "', " + 
            "'" + this->nome + "', " + 
            "'" + this->cognome + "', " + 
            "'" + this->ente + "', " + 
            std::to_string(this->ruolo) + ");";

        //std::cout << myQ << std::endl;    //da salvare nel file di log

        const char *myQuery = myQ.c_str(); //devo convertire string --> char*
        MySQL_conn->executeQuery(con, myQuery);
        MySQL_conn->closeConnection(con, nullptr);
    };

};
    

