#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>

#define QUERY_SIZE 1000

namespace persistence::DAO::MySQL{

    MySQLConnection::MySQLConnection(){}

    MySQLConnection* MySQLConnection::getInstance() {
        if ( instance == nullptr ){
            instance = new MySQLConnection(); //creato dinamicamente
        }
        return instance;
    };

    MYSQL* MySQLConnection::getNewConnection(){

        MYSQL* connection = mysql_init(NULL);

        if(connection == NULL){
            std::cout << "mysql_init() failed! " << std::endl;
            exit(1);
        }

        if(mysql_real_connect(connection, "localhost", "apl_user", "20Apl22!", "GNSShare", 0, NULL, 0) == NULL){
            finishWithError(connection);
        }
        
        return connection;
    }

    void MySQLConnection::closeConnection(MYSQL* con, MYSQL_RES* result){
        if(result != nullptr) mysql_free_result(result);
        mysql_close(con);
    };

    void MySQLConnection::finishWithError(MYSQL* con)
    {
        std::string* errorMessage = new std::string(mysql_error(con));
        if( errorMessage->find("Duplicate entry") != std::string::npos ) *errorMessage = "SQL01 " + *errorMessage;
        std::cout << utility::textColor_red(*errorMessage) << std::endl;
        mysql_close(con);
        throw errorMessage;
        exit(1);    
    };

    MYSQL_RES* MySQLConnection::getQueryResult(MYSQL *con, MYSQL_RES *result, char const *query){
        if(mysql_query(con, query)) finishWithError(con);
        result = mysql_store_result(con);
        if(result == NULL) finishWithError(con);
    };

    void MySQLConnection::executeQuery(MYSQL *con, char const *query){
        if(mysql_query(con, query)) finishWithError(con);
    };

}