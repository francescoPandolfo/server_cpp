#include <mysql/mysql.h>

//classi specifiche per il database MySQL
namespace persistence::DAO::MySQL{

    class MySQLConnection{  //classe singleton per aprire una sola connessione col database
        static MySQLConnection* instance;
        MySQLConnection();
    public:
        static MySQLConnection* getInstance();
        MYSQL* getNewConnection();
        void closeConnection(MYSQL*, MYSQL_RES*);

        void finishWithError(MYSQL*);
        MYSQL_RES* getQueryResult(MYSQL*, MYSQL_RES*, const char*);
        void executeQuery(MYSQL*, const char*);
    };

    class Utente final : public persistence::DAO::Utente{
    public:
        Utente();
        Utente(std::string& usr, std::string& pwd, std::string& n, std::string& c, std::string& e, int& r) : persistence::DAO::Utente(usr, pwd, n, c, e, r){ std::cout <<"chiamato il costruttore di DAO::MySQL::Utente" << std::endl; };
        ~Utente(){ std::cout <<"chiamato il distruttore di DAO::MySQL::Utente" << std::endl; };

        void save() override;
    };

    class StazioneGNSS final : public persistence::DAO::StazioneGNSS{
    public:
        StazioneGNSS();
        StazioneGNSS(std::string& cod, std::string& nome, float& lat, float& lng, float& alt, std::string& rete, std::string& tipo, bool& gps, bool& gal, bool& glo) : persistence::DAO::StazioneGNSS(cod, nome, lat, lng, alt, rete, tipo, gps, gal, glo){};
        ~StazioneGNSS();

        void save() override;
    };

    class MetadatiFile final : public persistence::DAO::MetadatiFile{
    public:
        MetadatiFile();
        MetadatiFile(std::string& cs, std::string& n, std::string& dc, float& fr, std::string& i, std::string& f) : persistence::DAO::MetadatiFile(cs, n, dc, fr, i, f){};
        ~MetadatiFile();

        void save() override;
        void incrementaDownload() override;
    };

    class MovimentiFile final : public persistence::DAO::MovimentiFile{
    public:
        MovimentiFile();
        MovimentiFile(std::string& usr, std::string& tipo, std::string& nome) : persistence::DAO::MovimentiFile(usr, tipo, nome){};
        ~MovimentiFile();

        void save() override;
    };

    class _OggettiDAO{
    public:
        static std::list<persistence::DAO::Utente*>* estraiUtenti();
        static std::list<persistence::DAO::StazioneGNSS*>* estraiStazioniGNSS();
        static std::list<persistence::DAO::MetadatiFile*>* estraiFilesPerStazione(const std::string&);
        static std::map<std::string, int>* estraiCampiTabella(const std::string&);
    };

    MySQLConnection* MySQLConnection::instance = nullptr;

}