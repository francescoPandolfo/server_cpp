namespace persistence::DAO::MySQL{

    MetadatiFile::MetadatiFile(){
        std::cout <<"chiamato il costruttore di MetadatiFile_DAO_MySQL" << std::endl;
    }

    MetadatiFile::~MetadatiFile(){
        std::cout <<"chiamato il distruttore di MetadatiFile_DAO_MySQL" << std::endl;
    }

    void MetadatiFile::save(){ //metodo ereditato dalla superclasse DAO
        std::cout <<"chiamato il metodo save() di MetadatiFile_DAO_MySQL, per : " << this->nome << std::endl;

        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();
        MYSQL *con = MySQL_conn->getNewConnection();

        std::string myQ = "INSERT INTO MetadatiFile(codiceStazione, nome, dataCreazione, frequenza, inizioPeriodo, finePeriodo) VALUES("
            "'" + this->codStazione + "', " +
            "'" + this->nome + "', " +
            "'" + this->dataCreazione + "', " +
            std::to_string(this->frequenza) + ", " +
            "'" + this->inizioPeriodo + "', " +
            "'" + this->finePeriodo + "');";

        std::cout << myQ << std::endl;    //da salvare nel file di log

        const char *myQuery = myQ.c_str(); //devo convertire string --> char*
        MySQL_conn->executeQuery(con, myQuery);
        

        std::cout <<"chiamato il metodo save() di DAO::MySQL::MetadatiFile, per : " << this->nome << std::endl;

        MySQL_conn->closeConnection(con, nullptr);
    };

    void MetadatiFile::incrementaDownload(){

        MySQLConnection* MySQL_conn = MySQLConnection::getInstance();
        MYSQL *con = MySQL_conn->getNewConnection();

        std::string myQ = "UPDATE MetadatiFile SET downloads = downloads + 1 where codiceStazione = '" + this->codStazione + "' AND nome = '" + this->nome + "'";
        std::cout << myQ << std::endl;    //da salvare nel file di log

        const char *myQuery = myQ.c_str(); //devo convertire string --> char*
        MySQL_conn->executeQuery(con, myQuery);
        std::cout <<"chiamato il metodo incrementaDownload() di DAO::MySQL::MetadatiFile, per : " << this->nome << std::endl;

        MySQL_conn->closeConnection(con, nullptr);
    };

}
