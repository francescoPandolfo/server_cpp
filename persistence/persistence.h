#include <iostream>

namespace persistence{

    namespace DAO{
        
        class DAO{ //Data Access Object
        public:
            virtual ~DAO(){};   //se non lo implemento non verrà chiamato il distruttore delle classi derivate
            
            virtual void save(){    //lo dichiaro virtual in modo che sia chiamato il save della specifica classe derivata (in base al database utilizzato)
                std::cout <<"chiamato il metodo save() dell'interfaccia DAO" << std::endl;
            };
        };

        class Utente : public DAO{
        public:
            std::string username, password, nome, cognome, ente;
            int ruolo;

            Utente(){};
            Utente(std::string usr, std::string pwd, std::string n, std::string c, std::string e, int r){
                this->username = usr;
                this->password = pwd;
                this->nome = n;
                this->cognome = c;
                this->ente = e;
                this->ruolo = r;
            };
        };

        class StazioneGNSS : public DAO{ //interfaccia utilizzata dall'interfaccia DAOFactory
        public:
            std::string codice, nome, rete, tipo;
            float latitudine, longitudine, altitudine;
            bool gps, galileo, glonass;

            StazioneGNSS(){};
            StazioneGNSS(std::string cod, std::string nome, float lat, float lng, float alt, std::string rete, std::string tipo, bool gps, bool gal, bool glo){
                this->codice        = cod;
                this->nome          = nome;
                this->latitudine    = lat;
                this->longitudine   = lng;
                this->altitudine    = alt;
                this->rete          = rete;
                this->tipo          = tipo;
                this->gps           = gps;
                this->galileo       = gal;
                this->glonass       = glo;
            };
        };

        class MetadatiFile : public DAO{
        public:           
            std::string codStazione, nome, dataCreazione, inizioPeriodo, finePeriodo;
            float frequenza;

            MetadatiFile(){};
            MetadatiFile(std::string codStazione, std::string nome, std::string dataC, float freq, std::string inizio, std::string fine){
                this->codStazione   = codStazione;
                this->nome          = nome;
                this->dataCreazione = dataC;
                this->frequenza     = freq;
                this->inizioPeriodo = inizio;
                this->finePeriodo   = fine;
            };
            virtual void incrementaDownload(){};
        };

        class MovimentiFile : public DAO{
        public:           
            std::string username, tipoMovimento, nomeFile;

            MovimentiFile(){};
            MovimentiFile(std::string username, std::string tipoMovimento, std::string nomeFile){
                this->username      = username;
                this->tipoMovimento = tipoMovimento;
                this->nomeFile      = nomeFile;
            };
        };

    }
    
    class DAOFactory{
    protected:  //essendo un'interfaccia i metodi devono essere visibili solo nelle classi derivate
        //li dichiaro virtual in modo che siano chiamati i metodi implementati nelle rtispettive classe derivate
        virtual DAO::Utente* factoryUtente(){  std::cout <<"chiamata la factoryUtente dell'interfaccia DAOFactory" << std::endl; }; 
        virtual DAO::StazioneGNSS* factoryStazioneGNSS(){  std::cout <<"chiamata la factoryStazioneGNSS dell'interfaccia DAOFactory" << std::endl; };
        virtual DAO::MetadatiFile* factoryMetadatiFile(){ std::cout <<"chiamata la factoryMetadatiFile dell'interfaccia DAOFactory" << std::endl; };
        virtual DAO::MovimentiFile* factoryMovimentiFile(){ std::cout <<"chiamata la factoryMovimentiFile dell'interfaccia DAOFactory" << std::endl; };

    //metotodi che non devono essere sovrascritti dalle classi derivate
        virtual void salvaUtente_DAO() final; 
        virtual void salvaStazioneGNSS_DAO() final; 
        virtual void salvaMetadatiFile_DAO() final;
        virtual void salvaMovimentiFile_DAO() final;
        virtual void incrementaDownload_DAO() final;        
    };    

}