namespace persistence{

    void DAOFactory::salvaUtente_DAO(){
        DAO::Utente* ut = factoryUtente();
        ut->save();
        delete ut;
    };

    void DAOFactory::salvaStazioneGNSS_DAO(){
        DAO::StazioneGNSS *st = factoryStazioneGNSS();
        st->save();
        delete st;
    };

    void DAOFactory::salvaMetadatiFile_DAO(){
        DAO::MetadatiFile *mf = factoryMetadatiFile();
        mf->save();
        delete mf;
    };

    void DAOFactory::salvaMovimentiFile_DAO(){
        DAO::MovimentiFile *mf = factoryMovimentiFile();
        mf->save();
        delete mf;
    };

    void DAOFactory::incrementaDownload_DAO(){
        DAO::MetadatiFile *mf = factoryMetadatiFile();
        mf->incrementaDownload();
        delete mf;
    };
    
}